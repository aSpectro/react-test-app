function Links(props) {
  return (
      <a className="menu-a" href="{props.hrefs}">{props.title}</a>
  );
}

function Header(props) {
  return (
  <header>
    <div className="container">
      <a href="/" className="logo">{props.logoName}</a>
      <menu>
        <Links title="Главная" hrefs="/" />
        <Links title="Ссылка1" hrefs="#" />
        <Links title="Ссылка2" hrefs="#" />
      </menu>
    </div>
  </header>
  );
}

function App() {
  return (
      <Header logoName="Logotype" />
  );
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);
